import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CzatComponent} from '../app/czat/czat.component';
import {LoginComponent} from '../app/login/login.component';
import {ZadzwonComponent} from '../app/zadzwon/zadzwon.component';
const routes: Routes = [
  {path :'Czat',component:CzatComponent},
  {path :'Zadzwon',component:ZadzwonComponent},
  {path :'Login',component:LoginComponent},
  {path :'',component:LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
