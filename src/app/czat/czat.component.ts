import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {SocketioService} from '../socketio.service'
import { interval } from 'rxjs';
@Component({
  selector: 'app-czat',
  templateUrl: './czat.component.html',
  styleUrls: ['./czat.component.css']
})
export class CzatComponent implements OnInit {  
  wiad :string="";
  sendwiad = "";
  showmess="";
  constructor(private activatedRoute: ActivatedRoute ,private socketService: SocketioService) { 
    this.activatedRoute.queryParams.subscribe(params => {
      user = params['user'];
      console.log(user);
    });
    interval(100).subscribe(() => {
      console.log("sprawdzam");
      this.checkMesseageChange();
  });
    }

  ngOnInit(): void {
    this.socketService.setupSocketConnection(user);
  }
  checkMesseageChange()
  {
    if(this.socketService.messFromServer!=undefined )
    {
      if(this.showmess!=this.socketService.messFromServer.text )
      {
        showMesseage(this.socketService.messFromServer);
        this.showmess=this.socketService.messFromServer.text;
      }
    }
  }
  sendMess()
  {
    this.socketService.sendMesseage(this.wiad);
    this.sendwiad=this.wiad;
    this.wiad="";
  }

}
let user;

function showMesseage(msg) {
  const div = document.createElement('div');
  if (msg.username !=null && msg.username == user) {
      div.classList.add('message-you');  
      div.innerHTML = `<p class="text"> ${msg.text} </p>`;     
  }
  else {
      div.classList.add('message');
      div.innerHTML = `<p> ${msg.text} </p>`;
  }
  
  document.querySelector('.czat-messages').appendChild(div);
}
