import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  nazwa: string = '';
  condition: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }
  onKey(event: any) { // without type info
    this.isNotEmpty();
  }
  isNotEmpty() {
    if (this.nazwa.length > 0) {
      this.condition = true;
    }
    else {
      this.condition = false;
    }
  }


}
