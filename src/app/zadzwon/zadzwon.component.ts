import { Component, OnInit } from '@angular/core';
import { CallService } from '../call.service'
@Component({
  selector: 'app-zadzwon',
  templateUrl: './zadzwon.component.html',
  styleUrls: ['./zadzwon.component.css']
})
export class ZadzwonComponent implements OnInit {

  number: string
  validator = /(^[0-9]{9}$)/
  state: string = "waiting"
  constructor(private callService: CallService) { }
  ngOnInit(): void { }
  call() {
    if (this.isValidNumber()) {
      this.callService.placeCall(this.number)
      this.callService.getCallId().subscribe(id => {
        this.checkStatus()
      })
    } else {
      console.info("Numer niepoprawny")
    }
  }
  isValidNumber(): Boolean {
    return this.validator.test(this.number)
  }
  checkStatus() {
    this.callService.getCallStatus().subscribe(state => {
      this.state = state
    })
  }
}


