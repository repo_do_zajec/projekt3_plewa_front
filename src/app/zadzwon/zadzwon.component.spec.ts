import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZadzwonComponent } from './zadzwon.component';

describe('ZadzwonComponent', () => {
  let component: ZadzwonComponent;
  let fixture: ComponentFixture<ZadzwonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZadzwonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZadzwonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
