import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})

export class SocketioService {
messFromServer;
  constructor() { }
  setupSocketConnection(uzytkownik) {
    socket = io('http://localhost:3000');  
    socket.emit('uzytkownikDolaczyl',uzytkownik);  
    socket.on('wiadomosc', msg => {
      this.messFromServer=msg;
  });
  }
  sendMesseage(msg)
  {
    socket.emit('chatMesseage', msg);
  }
}
let socket;


