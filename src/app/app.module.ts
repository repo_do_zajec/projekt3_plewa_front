import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CzatComponent } from './czat/czat.component';
import { LoginComponent } from './login/login.component';
import { ZadzwonComponent } from './zadzwon/zadzwon.component';
import { SocketioService } from './socketio.service';
import { CallService } from './call.service'
import { HttpClientModule } from '@angular/common/http'
@NgModule({
  declarations: [
    AppComponent,
    CzatComponent,
    LoginComponent,
    ZadzwonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [SocketioService,CallService],
  bootstrap: [AppComponent]
})
export class AppModule { }
